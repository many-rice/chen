package com.information.info.controller.front;

import com.information.info.entity.Info;
import com.information.info.service.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("/information/front")
public class InfoFrontController {
    @Autowired
    private InfoService infoService;

    /**
     * 查询flag=1（在展示状态）资讯
     * @return
     */
    @GetMapping("/infos")
    public Iterable<Info> online_infos(){
        return infoService.infos(new Integer(1));
    }

    /**
     * 按id查询（在展示状态）资讯
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public Info online_info(@PathVariable("id") Long id){
        return infoService.infoByflag(id,new Integer(1));
    }
}
