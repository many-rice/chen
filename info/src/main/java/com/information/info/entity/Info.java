package com.information.info.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name="info")
public class Info {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name="title")
    private String title;
    @Column(name="name")
    private String name;
    @Column(name="date")
    private String date;
    @Column(name="content")
    private String content;
    @Column(name="album")
    private String album;
    @Column(name="flag")
    private Integer flag;
    @Column(name="sort")
    private Long sort;

    @Tolerate
    public Info(){

    }
}
